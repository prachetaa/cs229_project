import collections
import sys

allUsers = set()

foul = collections.Counter()
reptr = collections.Counter()
for line in open(sys.argv[1],'r'):
    line = line.strip().split(';')

    allUsers.add(line[1])
    allUsers.add(line[2])
    
    if line[1] == line[8]:
        foul[line[2]] +=1
        reptr[line[1]] += 1
    elif line[2] == line[8]:
        foul[line[1]] += 1
        reptr[line[2]] += 1
    else:
        pass
        #print line[1], line[2], line[8]

for badUser in foul:
    allUsers.remove(badUser)

f =open('data/KnownGoodies1.txt', 'w')
c = 0
for l in open(sys.argv[2],'r'):
    if c > 200000:
        break
    line =  l.strip().split(';')
    if line[1] in allUsers or line[2] in allUsers:
        f.write(l)
        c+=1
