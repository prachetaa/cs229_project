import sys
import collections
from scipy import sparse
import ast
import numpy as np
import marshal

#
# 925240
# 18101426
#

from collections import Counter
o = open('wordCounts.pickle','w')
wordC = [Counter(), Counter()]
fileName = [sys.argv[1],sys.argv[2]]
ic= 0
for f in range(2):
    takeExamples = 0
    for line in open(fileName[f],'r'):
        ic += 1

        line = line.strip().split(';')
        if line[-3] == '{}' and int(line[-4]) == 0:
            assert 1==2
            continue
        wv= [line[-1], line[-2]]
        user = [line[1], line[2]]

        reported = line[-6]
        for i in [0,1]:
            for (k,v) in ast.literal_eval(wv[i].replace('"','\'')).items():
                c = int(k.split(':')[1])
                wordC[f][c] += v
        if ic %10000==0:
            print ic
        takeExamples += 1
        if takeExamples> 1000000:
            break
    print 'Processed', fileName[f]
    print 'Total Word Count:', sum(wordC[f])
    print 'Total Word:', len(wordC[f])
    print 'Most Common', wordC[f].most_common(20)

finalC={}
totalC = wordC[0]+wordC[1]
myIndex = 0
i = 0
for entry in totalC.most_common(900000):
    word, count = entry
    frac = (wordC[0][word]*1.0+1)/(wordC[1][word]*1.0+1)
    if frac > 1:
       frac = 1.0/frac
    if frac > 0.65:
       continue
    finalC[word] = myIndex
    myIndex += 1
print len(finalC)
marshal.dump(finalC, o)
o.close()
