from collections import defaultdict
import ast
import marshal
#profile:4545;United States;False;16;F;2013-09-12T02:03:47.504187;{"word:57": 1, "word:54": 1, "word:1926": 1, "word:39": 1, "word:970": 1, "word:134": 2, "word:294": 1, "word:648": 1, "word:253": 1, "word:1404": 1, "word:22": 1, "word:925241": 2, "word:925240": 1, "word:803": 2, "word:121": 2, "word:5446": 1, "word:229": 2, "word:1221": 1, "word:484017": 2, "word:2664": 1, "word:430": 1, "word:3840": 2, "word:3": 2, "word:261": 1};{"word:55033": 1}


f = open('data/profiles_dataset.csv', 'r')
profiles = defaultdict(lambda : {})
for line in f:
    line = line.strip().split(';')
    if line[0][0] == '#':
        continue 
    profiles[line[0]]['country'] = line[1]
    profiles[line[0]]['about'] = ast.literal_eval(line[-2].replace('"','\''))
    profiles[line[0]]['screenName'] = line[-1]
    profiles[line[0]]['age'] = line[3]
    profiles[line[0]]['sex'] = line[4]
    profiles[line[0]]['lf'] = line[2]
    profiles[line[0]]['disconnect'] = 0
    profiles[line[0]]['reported'] = 0
    profiles[line[0]]['f'] = 0
    profiles[line[0]]['s'] = 0
    profiles[line[0]]['l'] = 0
    profiles[line[0]]['total'] = 0
    
f = open('data/tmp/second_pass_','r')
for line in f:
    line = line.strip().split(';')

    user = [line[1], line[2]]
    profile = [line[3], line[4]]
    reported = line[8]
    lengths = [line[10], line[11]]
    f_flag = line[-1]
    disconnect = line[7]

    if disconnect == user[1]:
        profiles[profile[0]]['disconnect'] += 1
    elif disconnect== user[0]:
        profiles[profile[1]]['disconnect'] += 1

    if reported != user[1]:
        profiles[profile[1]]['reported'] += 1
    elif reported != user[0]:
        profiles[profile[0]]['reported'] += 1

    profiles[profile[0]][f_flag] += 1
    profiles[profile[1]][f_flag] += 1
    
    profiles[profile[0]]['total'] += 1
    profiles[profile[1]]['total'] += 1

o = open('data/profiles.pickle','w')

print profiles['profile:4545']
profiles = dict(profiles)
print 'Pickling'
marshal.dump(profiles,o)
