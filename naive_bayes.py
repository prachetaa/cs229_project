import sys
import collections
from scipy import sparse
import ast
import numpy as np
import marshal
import pylab as pl
#
# 925240
# 18101426
#
#wordC = pickle.load(open('wordCounts.pickle','r'))
#totalC = dict(wordC[0].most_common(10) + wordC[1].most_common(10))
#print totalC
#print 'Loaded Word Counts...',len(wordC[0])

profiles = marshal.load(open('data/profiles.pickle','r'))
print 'Loaded Profile Pickles....'

#wordMap = marshal.load(open('wordCounts.pickle','r'))
#print 'Loaded Map pickles...', len(wordMap), wordMap.items()[1]

C = 925240*2+1#2*len(wordMap) + 16
#C = 2*925240
R = 2*(int(sys.argv[3]))
X = sparse.lil_matrix((R, C))
Y = np.zeros(R)
r=0

fileName = [sys.argv[1],sys.argv[2]]
trainFileName = [x+'.train' for x in fileName]
testFileName = [x+'.test' for x in fileName]
for f in range(2):
    for line in open(fileName[f],'r'):
        line = line.strip().split(';')
        if int(line[10]) == 0 or int(line[11]) == 0:
            assert 1==2
            continue
        wv= [line[12], line[13]]
        user = [line[1], line[2]]
        profile = [line[3], line[4]]
        reported = line[8]
        for i in [0,1]:
            for (k,v) in ast.literal_eval(wv[i].replace('"','\'')).items():
                c = int(k.split(':')[1])
                #if c not in wordMap:
                #   continue
                #else:
                #   c = wordMap[c]
                if c< C:
                   X[r,c] = v
                else:
                   pass
                    #print c
            if profile[i] in profiles:
                for (k,v) in profiles[profile[i]]['about'].items():
                    c = int(k.split(':')[1])
                    #if c not in wordMap:
                    #    continue
                    #else: 
                    #    c = wordMap[c]
                    if (C/2)+c<C: 
                        X[r,c+(C/2)] += v
        X[r, C-1] = profiles[profile[0]]['sex'] == 'M'
        X[r, C-2] = profiles[profile[1]]['sex'] == 'M'
        #if profiles[profile[0]]['age'] != 'None':
        #    X[r, C-3] = int(profiles[profile[0]]['age'])
        #if profiles[profile[1]]['age'] != 'None':
        #    X[r, C-4] = int(profiles[profile[1]]['age'])
        X[r, C-5] = profiles[profile[0]]['lf'] == 'True'
        X[r, C-6] = profiles[profile[1]]['lf'] == 'True'
        Y[r] = f
#        if Y[r] == 0:
#            Y[r]=-1
        r+=1
        if r%1000 ==0:
           sys.stdout.write('.')
           sys.stdout.flush()
        if r >= (f*R/2)+R/2:
            break
    print 'Processed', fileName[f], R, r
#print total
from sklearn.feature_selection import chi2, SelectKBest
ch2 = SelectKBest(chi2, k=500)
X_train = ch2.fit_transform(X,Y)
print 'Finished feature selection....'
selection = ch2.get_support(True).tolist()
print type(selection),selection
marshal.dump(selection, open('featureSelectionResults1.500.pickle','w'))
print 'Saved Features'
'''
#print X
#print sum(Y)
from sklearn.naive_bayes import MultinomialNB, BernoulliNB
#from sklearn.ensemble.sparse import RandomForestClassifier
#from sklearn.svm.sparse import SVC
clf = MultinomialNB()
#clf = RandomForestClassifier(n_estimators=10)
#clf = SVC(C=1.0, cache_size=20000, coef0 = 0.0, gamma=0.5,kernel='poly',probability=False, scale_C=False, shrinking=True, tol=0.001)
print 'Training....'
clf.fit(X_train, Y)
print 'Done....'

#plot_decision_function()
X_train = X_train.tocsr()
r = 0
correct_0 =0
correct_1 =0
correct =0
'''
'''
for f in range(2):
    for line in open(fileName[f],'r'):
        Z = sparse.lil_matrix((1,C))
        line = line.strip().split(';')

        wv= [line[-1], line[-2]]
        user = [line[1], line[2]]
        profile = [line[3],line[4]]
        reported = line[-6]
        for i in [0,1]:
            for (k,v) in ast.literal_eval(wv[i].replace('"','\'')).items():
                c = int(k.split(':')[1])
                if c not in wordMap:
                    continue
                else:
                    c = wordMap[c]
                Z[0,c] = v
            if profile[i] in profiles:
                for (k,v) in profiles[profile[i]]['about'].items():
                    c = int(k.split(':')[1])
                    if c not in wordMap:
                        continue
                    else:
                        c = wordMap[c]
                    if c+(C/2)<C:
                        X[r,c+(C/2)] += v
        Z[0, C-1] = profiles[profile[0]]['sex'] == 'M'
        Z[0, C-2] = profiles[profile[1]]['sex'] == 'M'
        #if profiles[profile[0]]['age'] != 'None':
        #    Z[0, C-3] = int(profiles[profile[0]]['age'])
        #if profiles[profile[1]]['age'] != 'None':
        #    Z[0, C-4] = int(profiles[profile[1]]['age'])
        Z[0, C-5] = profiles[profile[0]]['lf'] == 'True'
        Z[0, C-6] = profiles[profile[1]]['lf'] == 'True'
        '''
'''
for r in range(R):
    if True:
        Z = X_train[r,:]
        f = Y[r]
        #Z = ch2.transform(X_train[r,:])
        correct+=int ( f== clf.predict(Z))
        if f == 1:
            correct_1 += int ( f== clf.predict(Z))
        else:
            correct_0 += int ( f== clf.predict(Z))
        r+=1
        if r%100 == 0:
            sys.stdout.write('.')
            sys.stdout.flush()
        #if r >=(f+1)*500:
        #    break
print 'Training Set:', r
print 'Nonspam correctly predicted', correct_0
print 'Spam correctly predicted', correct_1
print 'Total correctly',correct
#print clf.predict(X[481,:])
#print clf.predict(X[482,:])
'''
