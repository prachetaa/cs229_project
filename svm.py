import sys
import collections
from scipy import sparse
import ast
import numpy as np
import marshal
#from milk.supervised import randomforest
from random import randint
#
# 925240
# 18101426
#

ultraBadies = marshal.load(open('ultraBadPeople1.36.pickle','r'))
ultraBadies = set([x[0] for x in ultraBadies ])
print '\n\nLength of baddies ...', len(ultraBadies)
print sys.argv[4]
wordSet = marshal.load(open('featureSelectionResults1.1000.pickle','r'))
#wordSet = [ randint(0, 925240)  for i in range(1000)]
print 'Number of features', len(wordSet)
#wordMap = marshal.load(open('wordCounts.pickle','r'))
#print 'Loaded Map pickles...', len(wordMap), wordMap.items()[1]
#W = len(wordMap)
C = len(wordSet)+20 #+ 2*W + 6
R = int(sys.argv[3])

X = sparse.lil_matrix((R, C))
#X = np.zeros((R, C))
Y = np.zeros(R)
r=0
#wordC = pickle.load(open('wordCounts.pickle','r'))
#totalC = dict(wordC[0].most_common(10) + wordC[1].most_common(10))
#print totalC
#print 'Loaded Word Counts...',len(wordC[0])

profiles = marshal.load(open('data/profiles.pickle','r'))
print 'Loaded Profile Pickles....'

fileName = [sys.argv[1],sys.argv[2]]
first = True
for f in range(2):
    takeExamples = 0
    for line in open(fileName[f],'r'):
        line = line.strip().split(';')
        if int(line[10]) == 0 or int(line[11]) == 0:
            assert 1==2
            continue
        wv= [line[12], line[13]]
        user = [line[1], line[2]]
        profile = [line[3], line[4]]
        reported = line[8]
        lengths = [line[10], line[11]]
        f_flag = line[-1]
        if first:
            print 'Word Vectors', wv
            print 'users', user
            print 'profile',profile
            print 'reported', reported
            print 'lengths', lengths
            first = False
        for i in [0,1]:
            for (k,v) in ast.literal_eval(wv[i].replace('"','\'')).items():
                c = int(k.split(':')[1])
                #if c not in wordMap:
                #    continue
                #else:
                #    c = wordMap[c]
                
                if c not in wordSet:
                    continue
                else:
                    c = wordSet.index(c)
                X[r,c] = v
            if profile[i] in profiles:
                for (k,v) in profiles[profile[i]]['about'].items():
                    c = int(k.split(':')[1])
                    #if c not in wordMap:
                    #    continue
                    #else:
                    #    c = wordMap[c]
                    if c not in wordSet:
                        continue
                    else: 
                        c = wordSet.index(c)
                    if (C/2)+c<C: 
                        X[r,c+(C/2)] += v
        X[r, C-1] = int(profiles[profile[0]]['sex'] == 'M')
        X[r, C-2] = int(profiles[profile[1]]['sex'] == 'M')
        #if profiles[profile[0]]['age'] != 'None':
        #    X[r, C-3] = int(profiles[profile[0]]['age'])
        #if profiles[profile[1]]['age'] != 'None':
        #    X[r, C-4] = int(profiles[profile[1]]['age'])
        #X[r, C-5] = profiles[profile[0]]['lf'] == 'True'
        #X[r, C-6] = profiles[profile[1]]['lf'] == 'True'
        X[r, C-5] = (profiles[profile[0]]['disconnect'] > (0.65*profiles[profile[0]]['total']))
        X[r, C-6] = (profiles[profile[1]]['disconnect'] > (0.65*profiles[profile[1]]['total']))
        X[r, C-9] = (profiles[profile[0]]['reported'] > (0.65*profiles[profile[0]]['total']))
        X[r, C-10] = (profiles[profile[1]]['reported'] > (0.65*profiles[profile[1]]['total']))
        X[r, C-7] = int(reported==user[0])
        X[r, C-8] = int(reported==user[1])
        #X[r, C-9] = int(lengths[0]) 
        #X[r, C-10] = int(lengths[1]) 
        X[r, C-11] = int(f_flag=='f')
        X[r, C-12] = int(f_flag=='l')# or f_flag=='s')
        X[r, C-13] = int(f_flag=='s')
	#X[r, C-14] = int(profiles[profile[0]]['country'] == profiles[profile[1]]['country'])
        Y[r] = f
        if Y[r] == 0:
            Y[r]=-1
        r+=1
        if r%1000 ==0:
           sys.stdout.write('.')
           sys.stdout.flush()
        takeExamples+= 1
        if takeExamples >= R/2:
            break
    print 'Processed', fileName[f], R, r
#print total

#print X
#print sum(Y)
from sklearn.naive_bayes import MultinomialNB, BernoulliNB
#from sklearn.ensemble.sparse import RandomForestClassifier
from sklearn.svm import SVC
#clf = MultinomialNB()
#clf = RandomForestClassifier(n_estimators=10)
clf = SVC(C=0.001, cache_size=20000, coef0 = 0.0, degree=2, gamma=0.5, kernel='poly',probability=False,  shrinking=True, tol=0.001)
#from milk.supervised.multi import one_against_one
#rf_learner = randomforest.rf_learner()
#learner = one_against_one(rf_learner)
#model = learner.train(X,Y)
print 'Training....'
clf.fit(X, Y)
print 'Done....'
import pickle
pickle.dump(clf, open('workingSVM.pickle','w'))

#plot_decision_function()
X = X.tocsr()
r = 0
correct_0 =0
correct_1 =0
correct =0
total_1 = 0
total_0 = 0
#testFileName = [x+'.test' for x in fileName]
#for f in range(2):
#    takeExamples = 0
first = True
if True:
    for line in open(sys.argv[4],'r'):
        Z = sparse.lil_matrix((1,C))
        line = line.strip().split(';')

        wv= [line[12], line[13]]
        user = [line[1], line[2]]
        profile = [line[3],line[4]]
        reported = line[8]
        lengths = [line[10], line[11]]
        f_flag = line[-1]
        if first:
            print 'Word Vectors', wv
            print 'users', user
            print 'profile',profile
            print 'reported', reported
            print 'lengths', lengths
            first = False
 
        for i in [0,1]:
            for (k,v) in ast.literal_eval(wv[i].replace('"','\'')).items():
                c = int(k.split(':')[1])
                #if c not in wordMap:
                #    continue
                #else:
                #    c = wordMap[c]

                if c not in wordSet:
                    continue
                else:
                    c = wordSet.index(c)
                Z[0,c] = v
            if profile[i] in profiles:
                for (k,v) in profiles[profile[i]]['about'].items():
                    c = int(k.split(':')[1])
                    #if c not in wordMap:
                    #    continue
                    #else:
                    #    c = wordMap[c]
                    if c not in wordSet:
                        continue
                    else:
                        c = wordSet.index(c)
                    if c+(C/2)<C:
                        Z[0,c+(C/2)] += v
        Z[0, C-1] = int(profiles[profile[0]]['sex'] == 'M')
        Z[0, C-2] = int(profiles[profile[1]]['sex'] == 'M')
        #if profiles[profile[0]]['age'] != 'None':
        #    Z[0, C-3] = int(profiles[profile[0]]['age'])
        #if profiles[profile[1]]['age'] != 'None':
        #    Z[0, C-4] = int(profiles[profile[1]]['age'])
        #Z[0, C-5] = profiles[profile[0]]['lf'] == 'True'
        #Z[0, C-6] = profiles[profile[1]]['lf'] == 'True'
        Z[0, C-5] = (profiles[profile[0]]['disconnect'] > (0.65*profiles[profile[0]]['total']))
        Z[0, C-6] = (profiles[profile[1]]['disconnect'] > (0.65*profiles[profile[1]]['total']))
        Z[0, C-9] = (profiles[profile[0]]['reported'] > (0.65*profiles[profile[0]]['total']))
        Z[0, C-10] = (profiles[profile[1]]['reported'] > (0.65*profiles[profile[1]]['total']))
        Z[0, C-7] = int(reported==user[0])
        Z[0, C-8] = int(reported==user[1])
        #Z[0, C-9] = int(lengths[0]) 
        #Z[0, C-10] = int(lengths[1]) 
        Z[0, C-11] = int(f_flag=='f')
        Z[0, C-12] = int(f_flag=='l')# or f_flag=='s')
        Z[0, C-13] = int(f_flag=='s')
	#Z[0, C-14] = int(profiles[profile[0]]['country'] == profiles[profile[1]]['country'])
 
        f = int((user[0] in ultraBadies) or (user[1] in ultraBadies))
        if f == 0:
            f = -1
#for r in range(R):
#    if True:
#        f = Y[r]
#        prediction = clf.predict(X[r,:])
#        #prediction = model.apply(Z)
#        #if prediction ==-1:
#        #    prediction = 0
        prediction = clf.predict(Z)
        correct+=int ( f== prediction)
        if f == 1:
            total_1 += 1
            correct_1 += int ( f== prediction)
        else:
            total_0 += 1
            correct_0 += int ( f== prediction)
        r+=1
        if r%100 == 0:
            sys.stdout.write('.')
            sys.stdout.flush()
        #takeExamples +=1  
        #if takeExamples >=(f+1)*500:#R/2-1:
        #    break
print 'Test Set:', r
print 'Total NonSpam in Test set', total_0
print 'Nonspam correctly predicted', correct_0
print 'Total Spam in Test set',total_1
print 'Spam correctly predicted', correct_1
print 'Total correctly',correct
#print clf.predict(X[481,:])
#print clf.predict(X[482,:])
