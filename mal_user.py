import collections
import ast
import sys
from scipy import sparse
import numpy as np

allUsers = set()

foul = collections.Counter()
for line in open('data/no_report.txt','r'):
    line = line.strip().split(';')

    allUsers.add(line[1])
    allUsers.add(line[2])
    
    if line[1] == line[8]:
        foul[line[2]] +=1
    elif line[2] == line[8]:
        foul[line[1]] += 1
    else:
        pass
        #print line[1], line[2], line[8]
print 'Total Users', len(allUsers)

goodPeople = set()
badPeople = set([ x[0] for x in foul.most_common(40)])
for user in allUsers:
    if foul[user] == 0:
        goodPeople.add(user)

print 'Got baddies', len(badPeople)
print 'Good Users ', len(goodPeople)
print 'Training' 

C = 925240
R_MAX = 100000

X = sparse.lil_matrix((R_MAX, C))
Y = np.zeros(R_MAX)

r = 0
for line in open('data/no_blanks.txt'):
    if r>= R_MAX-1:
        break

    line = line.strip().split(';')
 
    wv= [line[-1], line[-2]]
    user = [line[1], line[2]]

    reported = line[-6]
    
    for i in [0,1]:
        try:
            if wv[i] == '{}':
                continue
            for (k,v) in ast.literal_eval(wv[i].replace('"','\'')).items():
                c = int(k.split(':')[1])
                X[r, c] = v

        except IndexError:
            print r,c, R_MAX, C
        if wv[i] != '{}' and user[i] in goodPeople:
            Y[r] = 0
        elif wv[i] != '{}' and user[i] in badPeople:
            Y[r] = 1
        r+=1
	if r>= R_MAX-1:
            break

X = X.tocsr()

from sklearn.naive_bayes import MultinomialNB, BernoulliNB
clf = MultinomialNB()
clf.fit(X, Y)


r = 0
correct_0 =0
correct_1 =0
correct =0

for r in range(R_MAX):
    predicted = clf.predict(X[r,:])
    correct+=int ( Y[r]== predicted)
    if Y[r] == 1:
        correct_1 += int ( Y[r]== predicted)
    else:
        correct_0 += int ( Y[r]== predicted)
    r+=1
    if r%1000 == 0:
        print r
print 'Training Set:', r
print 'Nonspam correctly predicted', correct_0
print 'Spam correctly predicted', correct_1
print 'Total correctly',correct
  
