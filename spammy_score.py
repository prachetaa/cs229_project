from collections import Counter, defaultdict
import sys
import ast

spam_C = defaultdict(lambda : 1)
real_C = defaultdict(lambda : 1)

total_real =0
total_spam =0
diction= set()
for line in open(sys.argv[1],'r'):
    line = line.strip().split(';')
    if line[-3] == '{}' and int(line[-4]) == 0:
        assert 1==2
        continue
    wv= [line[-1], line[-2]]
    user = [line[1], line[2]]

    reported = line[-6]
    for i in [0,1]:
        bad_person = (reported==user[i])
        for (k,v) in ast.literal_eval(wv[i].replace('"','\'')).items():
            diction.add(k)
            if bad_person:
                spam_C[k] += v
                total_spam += v
            else:
                real_C[k] += v
                total_real += v
from math import log
total_spam = len(diction)
total_real = len(diction)

a = Counter()
for k in diction:
    a[k] = log(spam_C[k]*1.0/total_spam)-log(real_C[k]*1.0/total_real)

print a.most_common(5)
for p in a.most_common(5):
    k, s = p
    print k, spam_C[k]
