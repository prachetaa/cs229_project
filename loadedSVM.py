import sys
from sklearn.svm.sparse import SVC
from scipy import sparse
import pickle
import collections
import ast
import numpy as np
import marshal
clf = pickle.load(open('workingSVM.40baddies.pickle','r'))

ultraBadies = marshal.load(open('ultraBadPeople.40.pickle','r'))
ultraBadies = set([x[0] for x in ultraBadies ])
print 'Got pickle of ultraBadies...'

wordSet = marshal.load(open('featureSelectionResults.1000.pickle','r'))
#wordSet = [ randint(0, 925240)  for i in range(1000)]
print type(wordSet)
#wordMap = marshal.load(open('wordCounts.pickle','r'))
#print 'Loaded Map pickles...', len(wordMap), wordMap.items()[1]
#W = len(wordMap)
C = len(wordSet)+10#2*W + 6

#X = np.zeros((R, C))
r=0
#wordC = pickle.load(open('wordCounts.pickle','r'))
#totalC = dict(wordC[0].most_common(10) + wordC[1].most_common(10))
#print totalC
#print 'Loaded Word Counts...',len(wordC[0])

profiles = marshal.load(open('data/profiles.pickle','r'))
print 'Loaded Profile Pickles....'

correct_0 =0
correct_1 =0
correct =0
total_1 = 0
total_0 = 0

empty_wrong = 0
empty_total = 0
if True:
    for line in open(sys.argv[1],'r'):
        Z = sparse.lil_matrix((1,C))
        line = line.strip().split(';')

        wv= [line[-1], line[-2]]
        user = [line[1], line[2]]
        profile = [line[3],line[4]]
        reported = line[-6]
        lengths = [line[-3], line[-4]]
        for i in [0,1]:
            for (k,v) in ast.literal_eval(wv[i].replace('"','\'')).items():
                c = int(k.split(':')[1])
                if c not in wordSet:
                    continue
                else:
                    c = wordSet.index(c)
                Z[0,c] = v
            if profile[i] in profiles:
                for (k,v) in profiles[profile[i]]['about'].items():
                    c = int(k.split(':')[1])
                    if c not in wordSet:
                        continue
                    else:
                        c = wordSet.index(c)
                    if c+(C/2)<C:
                        Z[0,c+(C/2)] += v
        #Z[0, C-1] = profiles[profile[0]]['sex'] == 'M'
        #Z[0, C-2] = profiles[profile[1]]['sex'] == 'M'
        #if profiles[profile[0]]['age'] != 'None':
        #    Z[0, C-3] = int(profiles[profile[0]]['age'])
        #if profiles[profile[1]]['age'] != 'None':
        #    Z[0, C-4] = int(profiles[profile[1]]['age'])
        #Z[0, C-5] = profiles[profile[0]]['lf'] == 'True'
        #Z[0, C-6] = profiles[profile[1]]['lf'] == 'True'
        Z[0, C-7] = int(reported==user[0])
        Z[0, C-8] = int(reported==user[1])
        Z[0, C-9] = int(lengths[0]) 
        Z[0, C-10] = int(lengths[1]) 
        if '0' in lengths:
            empty_total += 1
        f = int((user[0] in ultraBadies) or (user[1] in ultraBadies))
        if f == 0:
            f = -1
#for r in range(R):
#    if True:
#        f = Y[r]
#        prediction = clf.predict(X[r,:])
#        #prediction = model.apply(Z)
#        #if prediction ==-1:
#        #    prediction = 0
        prediction = clf.predict(Z)
        if (f!=prediction) and ('0' in lengths):
            empty_wrong += 1

        #correct+=int( f== prediction)
        if f == 1:
            total_1 += 1
            correct_1 += int ( f== prediction)
        else:
            total_0 += 1
            correct_0 += int ( f== prediction)
        r+=1
        if r%100 == 0:
            sys.stdout.write('.')
            sys.stdout.flush()
        #takeExamples +=1  
        #if takeExamples >=(f+1)*500:#R/2-1:
        #    break
print 'Test Set:', r
print 'Total NonSpam in Test set', total_0
print 'Nonspam correctly predicted', correct_0
print 'Total Spam in Test set',total_1
print 'Spam correctly predicted', correct_1
print 'Total correctly',correct_0+correct_1
print 'Empty wrong', empty_wrong
print 'Total wrong', r-correct
print 'Total empty', empty_total
#
