from sklearn.svm.sparse import SVC
from scipy import sparse 

X = sparse.lil_matrix((2,2))
Y = [0,1]

clf = SVC()
clf = clf.fit(X,Y)

print clf.predict([1,1])

